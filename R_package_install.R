##ipak function: intall and load multiple R packages
## check to see if packages are installed. Install them if they are not, then load them into the R session.
## from https://gist.github.com/stevenworthington/3178163

ipak <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    install.packages(new.pkg, dependencies = TRUE)
  sapply(pkg, require, character.only = TRUE)
}

if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
bio_ipak <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    BiocManager::install(new.pkg)
  sapply(pkg, require, character.only = TRUE)
}

packages<-c("tibble","plyr","dplyr","igraph","writexl","Matrix","foreach","doParallel","iterators","bigmemory","gtools")
biopackages<-c("viper","fgsea","limma")
## install R packages with install.packages directly
ipak(packages)
## install R packages with BiocManager
bio_ipak(biopackages)

