## caseConversion

## convert mouse/rat gene symbol to human gene symbol
data_convert<-function(symbol,species){
  if(species=='human'){
    symbol_c<-symbol
  }else if(species=='rat'){
    syn<-readRDS('../database/rat2human.rds')
    symbol_c<-syn$HGNC.symbol[match(symbol,syn$RGD.symbol)]
    
  }else if(species=='mouse'){
    syn<-readRDS('../database/mouse2human.rds')
    symbol_c<-syn$HGNC.symbol[match(symbol,syn$MGI.symbol)]
    
  }else{
    print("Only support for species human, mouse and rat")
  }
  return(symbol_c)
}
